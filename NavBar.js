import React from "react"
import { Link } from 'react-router-dom';
import Login from "./Login";
import './Home.css'

const NavBar = () => {
        return (
            <div>
            <ul>
            <div className="Icon"><Link to="/"><li>Home</li></Link></div>
            <div className="Icon"> <Link to="/Login"><li>log in</li></Link></div>
            <div className="Icon"> <Link to="/Signup"><li>Signup</li></Link></div>
            <div className="Icon"><Link to="/About"><li>about</li></Link></div>
            </ul>
            </div>
        ) ;
}
export default NavBar;