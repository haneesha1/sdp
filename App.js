import React, {useState} from 'react'
const Button = (props) => {
return (
<button onClick={props.onClick}>
{props.text}
</button>
)
}
const Display = (props) => {
return (
<div>{props.counter}</div>
)
}
const App = () => {
const [ counter, setCounter ] = useState(0)
const increaseByOne = () => setCounter(counter + 1)
const decreaseByOne = () => setCounter(counter - 1)
return (
<div>
<h1 align="center">Counter</h1>
<center>
<Button
onClick={increaseByOne}
text='Increment'/>
&nbsp;&nbsp;&nbsp;&nbsp;
<Button
onClick={decreaseByOne}
text='Decrement'/>
</center>
<h2 align="center">Counter Value</h2>
<center><Display align="center"counter={counter}/></center>
</div>
)
}
export default App

