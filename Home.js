import React from 'react';
import logo from './images/event logo.jpg';
import cart from './images/icon.png';
import './Home.css';
import NavBar from './NavBar';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import { Button, Toolbar, Typography  } from '@material-ui/core';
import About from './About' ;
import Login from './Login';
import Signup from './Signup';

function Home () {

    return(
        <div>
        <div className="Home">-
            <img src={logo} alt="" height="120px" width="140px" />
            {/* <Toolbar position="sticky">   
                <Typography text-align="top-right"> selection / </Typography><br/>
                <Typography text-align="top-right">logout / </Typography><br/>
                <Typography text-align="top-right" >  Contact us / </Typography><br/> 
            </Toolbar>  */}
        <div className="head">
        <Router>
            <NavBar />
            <Switch>
                <Route path="/Login" component={Login} extract/>
                <Route path="/Signup" component={Signup}/>
                <Route path="/About" component={About}/>
                <Route path="./" component={Home} />
            </Switch>
        </Router>
        </div>
        </div>

            <div className="mid">
                <center>
                <h>Hash events </h><img src={cart} alt="" height="40px" width="40px" /><br/>
            <select>
                <option>events</option>
                <option>productlaunching</option>
                <option>conferences seminars</option>
                <option>weddings</option>
                <option>welcome parties</option>
                <option>office meetings</option>
                <option>birthday parties</option>
                <option>thanks giving</option>
                <option>sendoff parties</option>
                <option>festive parties</option>
            </select>
            <form>          
        <input type="text" placeholder="Search"/>
        <button type="submit">Q</button>
            </form>
            </center>
            
        </div>
        </div>
    );
}

export default Home;